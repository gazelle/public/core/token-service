/**
 * Gazelle Token Service is an Api for the Gazelle Test Bed
 * Copyright (C) 2006-2022 IHE
 * mailto :claude DOT lusseau AT kereval DOT com
 * <p>
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.token.interlay.service;

import net.ihe.gazelle.token.application.exception.UserNotFoundException;
import net.ihe.gazelle.token.domain.entity.Token;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 26/09/2022
 */
public class TokenServiceImplTest {

    private final TokenServiceImpl service = mock(TokenServiceImpl.class);
    private Token activeToken;
    private Token inactiveToken;
    private List<Token> doesTokens;

    @BeforeEach
    public void setUp() {
        activeToken = Token.builder()
                .withValue("csdfdsfdkslmfds")
                .withUsername("jdoe")
                .withOrganization("IHE")
                .withCreationDate(LocalDateTime.now().minusDays(300))
                .withExpirationDate(LocalDateTime.now().plusDays(30))
                .withRoles(List.of("admin_roles"))
                .build();
        inactiveToken = Token.builder()
                .withValue("lkfdjslkfjds")
                .withUsername("jdoe")
                .withOrganization("IHE")
                .withCreationDate(LocalDateTime.now().minusDays(300))
                .withExpirationDate(LocalDateTime.now().minusDays(30))
                .withRoles(List.of("admin_roles"))
                .build();
        doesTokens = new ArrayList<>();
        doesTokens.add(activeToken);
        doesTokens.add(inactiveToken);
    }

    @Test
    void findByValueTest() {
        when(service.findByValue(activeToken.getValue())).thenReturn(activeToken);
        Token token = service.findByValue("csdfdsfdkslmfds");
        Assertions.assertEquals("csdfdsfdkslmfds", token.getValue());
    }

    @Test
    void getUniqueActiveToken() {
        when(service.getUserTokens("jdoe")).thenReturn(doesTokens);
        List<Token> tokens = service.getUserTokens("jdoe");
        Optional<Token> tokenResult = tokens.stream()
                .filter(token -> token.getExpirationDate().isAfter(LocalDateTime.now()))
                .findFirst();
        Assertions.assertTrue(tokenResult.get().getExpirationDate().isAfter(LocalDateTime.now()));
    }

    @Test
    void deactivateValidTokenTest() {
        when(service.findByValue("csdfdsfdkslmfds")).thenReturn(activeToken);
        service.deactivateValidToken(activeToken.getValue());
        Token deactivateToken = service.findByValue(activeToken.getValue());
        Assertions.assertTrue(deactivateToken.getExpirationDate().isAfter(LocalDateTime.now()));
    }

    @Test
    void deleteOldTokensByUserTest() {
        when(service.getUserTokens("jdoe")).thenReturn(doesTokens);
        service.deleteOldTokensByUser("jdoe");
        Token token1 = service.findByValue("csdfdsfdkslmfds");
        Token token2 = service.findByValue("lkfdjslkfjds");
        Assertions.assertNull(token1);
        Assertions.assertNull(token2);
    }

    @Test
    void saveTokenTest() throws UserNotFoundException {
        service.saveToken("clusseau", "Kereval",List.of("admin_role"));
        List<Token> tokens = service.getUserTokens("clusseau");
        Assertions.assertNotNull(tokens);
    }
}

package net.ihe.gazelle.token.application.dao;

import net.ihe.gazelle.token.application.model.PreferenceEntity;
import net.ihe.gazelle.token.interlay.dao.exception.DaoException;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 14/07/2022
 */
public interface IPreferenceDAO {
    void update(PreferenceEntity preference) throws DaoException;
    void save(PreferenceEntity preference);
    PreferenceEntity getPreference(String id);
}
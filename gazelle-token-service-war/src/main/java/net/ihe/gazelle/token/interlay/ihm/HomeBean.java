
package net.ihe.gazelle.token.interlay.ihm;

import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import net.ihe.gazelle.token.interlay.factory.TokenInfrastructureFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 11/07/2022
 */
@Named
@RequestScoped
@LogAudited
public class HomeBean {


    private final TokenInfrastructureFactory serviceFactory;

    @Inject
    public HomeBean(TokenInfrastructureFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }


}

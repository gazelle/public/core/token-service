
package net.ihe.gazelle.token.application.model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token-service
 * @date 03/06/2022
 */

@Entity(name = "Token")
@Table(name = "gazelle_api_token")
@NamedQuery(name = "TokenEntity.findByUsername", query = "SELECT t FROM Token t WHERE t.username = :username ORDER BY t.creationDate DESC")
@NamedQuery(name = "TokenEntity.getAllPerOrganization", query = "SELECT t FROM Token t WHERE t.organization = :organization")
@NamedQuery(name = "TokenEntity.findAll", query = "SELECT t from Token t")
@NamedQuery(name = "TokenEntity.findByValue", query = "SELECT t FROM Token t WHERE t.value = :value")
@NamedQuery(name = "TokenEntity.findByOrganization", query = "SELECT t FROM Token t WHERE t.organization = :organization")
@NamedQuery(name = "TokenEntity.deleteByValue", query = "DELETE FROM Token t WHERE t.value = :value")
@NamedQuery(name = "TokenEntity.deactivateToken", query = "UPDATE Token t SET t.expirationDate = ?1 WHERE t.value = ?2")
@NamedQuery(name = "TokenEntity.deletePerUser", query = "DELETE FROM Token t WHERE t.expirationDate < :expirationDate AND t.username = :username ")
public class TokenEntity implements Serializable {
    @Id
    @Column(name = "value", nullable = false)
    private String value;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "organization", nullable = false)
    private String organization;

    @Column(name = "creation_date", nullable = false)
    private LocalDateTime creationDate;


    @Column(name = "expiration_date", nullable = false)
    private LocalDateTime expirationDate;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "gazelle_token_roles", joinColumns = @JoinColumn(name = "id"))
    @JoinColumn(name = "id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<String> roles = new ArrayList<>();

    public TokenEntity() {
    }

    public TokenEntity(TokenEntityBuilder builder) {
        this.value = builder.value;
        this.username = builder.username;
        this.organization = builder.organization;
        this.creationDate = builder.creationDate;
        this.expirationDate = builder.expirationDate;
        this.roles = builder.roles;
    }

    public static TokenEntityBuilder builder() {
        return new TokenEntityBuilder();
    }

    public static class TokenEntityBuilder {
        private String value;
        private String username;
        private String organization;
        private LocalDateTime creationDate;
        private LocalDateTime expirationDate;
        private List<String> roles;

        public TokenEntityBuilder withValue(final String value) {
            this.value = value;
            return this;
        }

        public TokenEntityBuilder withUsername(final String username) {
            this.username = username;
            return this;
        }

        public TokenEntityBuilder withOrganization(final String organization) {
            this.organization = organization;
            return this;
        }

        public TokenEntityBuilder withCreationDate(final LocalDateTime creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public TokenEntityBuilder withExpirationDate(final LocalDateTime expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public TokenEntityBuilder withRoles(final List<String> roles) {
            this.roles = roles;
            return this;
        }

        public TokenEntity build() {
            return new TokenEntity(this);
        }
    }

    public String getValue() {
        return value;
    }

    public String getUsername() {
        return username;
    }

    public String getOrganization() {
        return organization;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public List<String> getRoles() {
        return roles;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TokenEntity{");
        sb.append("value='").append(value).append('\'');
        sb.append(", username='").append(username).append('\'');
        sb.append(", organization='").append(organization).append('\'');
        sb.append(", creationDate=").append(creationDate);
        sb.append(", expirationDate=").append(expirationDate);
        sb.append(", roles=").append(roles);
        sb.append('}');
        return sb.toString();
    }
}






package net.ihe.gazelle.token.interlay.dao;

import net.ihe.gazelle.token.application.dao.IDaoFactory;
import net.ihe.gazelle.token.interlay.annotation.LogAudited;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 25/08/2022
 */
@Named("DaoFactoryImpl")
@LogAudited
public class DaoFactoryImpl implements IDaoFactory {

    private final EntityManager entityManager;

    @Inject
    public DaoFactoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    @Override
    @Named("PreferenceDaoImpl")
    public PreferenceDaoImpl getPreferenceDao() {
        return new PreferenceDaoImpl(entityManager);
    }

    @Override
    @Named("TokenDaoImpl")
    public TokenDaoImpl getTokenDao() {
        return new TokenDaoImpl(entityManager);
    }
}

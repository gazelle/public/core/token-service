
package net.ihe.gazelle.token.interlay.service;

import net.ihe.gazelle.token.application.model.EConstantValue;
import net.ihe.gazelle.token.domain.entity.Preference;
import net.ihe.gazelle.token.interlay.dao.TokenDaoImpl;
import net.ihe.gazelle.token.interlay.dao.exception.DaoException;
import org.junit.jupiter.api.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project GazelleTokenService
 * @date 19/06/2022
 */
class PreferenceServiceImplTest {

    private final PreferenceServiceImpl service = mock(PreferenceServiceImpl.class);
    private Preference preference;
    private Preference preference2;


    @BeforeEach
    public void init() {
        preference = Preference.builder()
                .withId(EConstantValue.getPreferenceId())
                .withTokenDuration(300)
                .build();
        preference2 = Preference.builder()
                .withId(EConstantValue.getPreferenceId())
                .withTokenDuration(200)
                .build();
    }

    @Test
    void getPreferenceTest() throws DaoException {
        when(service.getPreference()).thenReturn(preference);
        service.setDuration(preference.getTokenDuration());
        Preference preferenceTest = service.getPreference();
        Assertions.assertEquals(300, preferenceTest.getTokenDuration());
    }

    @Test
    void saveOrUpdatePreferenceTest() throws DaoException {
        when(service.getPreference()).thenReturn(preference);
        service.setDuration(preference.getTokenDuration());
        Preference preference1 = service.getPreference();
        Assertions.assertEquals(preference1.getTokenDuration(), preference.getTokenDuration());
        preference2 = Preference.builder()
                .withId(EConstantValue.getPreferenceId())
                .withTokenDuration(200)
                .build();
        service.setDuration(preference2.getTokenDuration());
        Assertions.assertEquals(200, preference2.getTokenDuration());
    }
}
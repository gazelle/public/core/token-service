
package net.ihe.gazelle.token.interlay.service;

import net.ihe.gazelle.token.application.service.*;
import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import net.ihe.gazelle.token.interlay.factory.TokenInfrastructureFactory;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 22/09/2022
 */
@LogAudited
@Named("ServiceFactoryImpl")
public class ServiceFactoryImpl implements IServiceFactory {

    private final TokenInfrastructureFactory factory;

    @Inject
    public ServiceFactoryImpl(TokenInfrastructureFactory factory) {
        this.factory = factory;
    }

    @Override
    public ITokenService getTokenService() {
        return new TokenServiceImpl(factory);
    }

    @Override
    public IPreferenceService getPreferenceService() {
        return new PreferenceServiceImpl(factory);
    }

    @Override
    public IPropertiesService getPropertiesService() {
        return new PropertiesServiceImpl();
    }

    @Override
    public IRestService getRestService() {
        return new RestServiceImpl(factory);
    }
}


package net.ihe.gazelle.token.application.model;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 14/07/2022
 */
@Entity(name = "Preference")
@Table(name = "gazelle_token_preference",
        uniqueConstraints = @UniqueConstraint(columnNames = {"preference_id"}))
@NamedNativeQuery(name = "PreferenceEntity.saveWithTokenDurationOnly", query = "INSERT INTO gazelle_token_preference (preference_id, token_duration) VALUES (:id, :duration)")
@NamedQuery(name = "PreferenceEntity.updateWithTokenDurationOnly", query = "UPDATE Preference p SET p.tokenDuration = :duration WHERE p.id = :id")
public class PreferenceEntity implements Serializable {

    @Id
    @Column(name = "preference_id", unique = true)
    private String id;

    @Column(name = "token_duration")
    private int tokenDuration;


    public PreferenceEntity(PreferenceEntityBuilder builder) {
        this.id = builder.id;
        this.tokenDuration = builder.tokenDuration;
    }

    public PreferenceEntity() {
    }

    public static PreferenceEntityBuilder builder() {
        return new PreferenceEntityBuilder();
    }

    public static class PreferenceEntityBuilder {
        private String id;
        private int tokenDuration;


        public PreferenceEntityBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public PreferenceEntityBuilder withTokenDuration(int tokenDuration) {
            this.tokenDuration = tokenDuration;
            return this;
        }

        public PreferenceEntity build() {
            return new PreferenceEntity(this);
        }
    }

    public String getId() {
        return id;
    }

    public int getTokenDuration() {
        return tokenDuration;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PreferenceEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", tokenDuration=").append(tokenDuration);
        sb.append('}');
        return sb.toString();
    }
}

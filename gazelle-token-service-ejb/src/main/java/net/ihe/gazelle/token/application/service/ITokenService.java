
package net.ihe.gazelle.token.application.service;

import net.ihe.gazelle.token.application.exception.UserNotFoundException;
import net.ihe.gazelle.token.domain.entity.Token;

import java.util.List;
import java.util.Optional;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 22/09/2022
 */

public interface ITokenService {
    void saveToken(String username, String organization, List<String> roles) throws UserNotFoundException;

    Token findByValue(String value);

    List<Token> getUserTokens(String username);

    List<Token> getAllTokenPerUser(String username);

    Optional<Token> getUniqueActiveToken(String username);

    void deactivateValidToken(String value);

    void deleteOldTokensByUser(String username);
}


package net.ihe.gazelle.token.interlay.dao;

import net.ihe.gazelle.token.application.dao.ITokenDAO;
import net.ihe.gazelle.token.application.model.TokenEntity;
import net.ihe.gazelle.token.interlay.annotation.EntityManagerProducer;
import net.ihe.gazelle.token.interlay.annotation.LogAudited;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 12/05/2022
 */
@Named("TokenDaoImpl")
@LogAudited
public class TokenDaoImpl implements ITokenDAO {

    private final EntityManager entityManager;

    @Inject
    public TokenDaoImpl(@EntityManagerProducer.InjectEntityManager EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void create(TokenEntity token) {
        entityManager.persist(token);
    }


    @Override
    public TokenEntity getTokenByValue(String value) {
        return entityManager.find(TokenEntity.class, value);
    }

    @Override
    public List<TokenEntity> getTokensPerOwner(String username) {
        Query query = entityManager.createNamedQuery("TokenEntity.findByUsername", TokenEntity.class);
        query.setParameter("username", username);
        return (List<TokenEntity>) query.getResultList();
    }

    @Override
    public void deleteOldTokens(String username) {
        Query query = entityManager.createNamedQuery("TokenEntity.deletePerUser");
        query.setParameter("expirationDate", LocalDateTime.now());
        query.setParameter("username", username);
        query.executeUpdate();
    }


    @Override
    public void deactivateValidToken(String value, LocalDateTime expirationDate) {
        Query query = entityManager.createNamedQuery("TokenEntity.deactivateToken");
        query.setParameter(1, expirationDate);
        query.setParameter(2, value);
        query.executeUpdate();
    }
}
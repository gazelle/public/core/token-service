
package net.ihe.gazelle.token.interlay.ihm;

import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import net.ihe.gazelle.token.interlay.factory.TokenInfrastructureFactory;
import net.ihe.gazelle.token.interlay.ihm.model.GetPrincipal;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 16/07/2022
 */
@Named
@ManagedBean
@LogAudited
public class PurgeBean implements Serializable {

    private final TokenInfrastructureFactory serviceFactory;

    @Inject
    public PurgeBean(TokenInfrastructureFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Inject
    private GetPrincipal principal;

    @Transactional
    public void deleteOldTokens() {
        this.serviceFactory.getServiceFactory().getTokenService().deleteOldTokensByUser(this.principal.getUsername());
    }
}

package net.ihe.gazelle.token.interlay.ihm;

import net.ihe.gazelle.token.application.service.IPreferenceService;
import net.ihe.gazelle.token.application.service.ITokenService;
import net.ihe.gazelle.token.domain.entity.Preference;
import net.ihe.gazelle.token.domain.entity.Token;
import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import net.ihe.gazelle.token.interlay.dao.exception.DaoException;
import net.ihe.gazelle.token.interlay.ihm.model.GetPrincipal;
import net.ihe.gazelle.token.interlay.ihm.util.GrowlBean;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 08/07/2022
 */
@Named
@LogAudited
public class AdminBean implements Serializable {

    private boolean admin = false;

    private int duration;
    private Preference preference;

    @Inject
    private GrowlBean growlBean;
    @Inject
    private IPreferenceService preferenceService;
    @Inject
    private ITokenService tokenService;
    @Inject
    private GetPrincipal getPrincipal;

    @PostConstruct
    public void init() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        if (getPrincipal.isAdmin()) {
            admin = true;
        } else {
            ExternalContext externalContext = context.getExternalContext();
            externalContext.redirect(externalContext.getRequestContextPath());
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Vous n'avez pas les droits necessaires pour accéder à cette partie de l'application"));
        }
    }

    public boolean isExpired(String value) {
        Token token = tokenService.findByValue(value);
        return !token.getExpirationDate().isBefore(LocalDateTime.now());
    }


    @Transactional
    public void setDuration(int value) throws DaoException {
        if (value > 365) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "La durée de vie des tokens ne peut exéder 365 jours"));
        }
        if (value < 1){
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "La durée de vie des tokens ne peut être inférieur à 1 jour"));
        }
        if (value <= 365 && value > 0) {
            this.preferenceService.setDuration(value);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"", "La durée de vie des tokens est désormais de " + value + " jours"));
        }
    }

    public int getDuration() {
        return this.preferenceService.getTokenDuration();
    }

    @Transactional
    public void deleteOldTokens() {
        String username = getPrincipal.getUsername();
        this.tokenService.deleteOldTokensByUser(username);
    }

    public boolean isAdmin() {
        return admin;
    }
}


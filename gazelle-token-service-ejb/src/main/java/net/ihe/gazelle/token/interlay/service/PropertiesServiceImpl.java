
package net.ihe.gazelle.token.interlay.service;

import net.ihe.gazelle.token.application.service.IPropertiesService;
import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import net.ihe.gazelle.token.interlay.conf.Configuration;
import net.ihe.gazelle.token.interlay.factory.TokenInfrastructureFactory;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 22/09/2022
 */
@LogAudited
@Named("PropertiesServiceImpl")
public class PropertiesServiceImpl implements IPropertiesService {

    @Override
    public String getVersion() {
        return Configuration.getinstance().getValue("version");
    }
}

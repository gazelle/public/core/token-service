
package net.ihe.gazelle.token.interlay.service;

import net.ihe.gazelle.token.application.service.IRestService;
import net.ihe.gazelle.token.application.utils.DateFormatter;
import net.ihe.gazelle.token.domain.entity.Token;
import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import net.ihe.gazelle.token.interlay.factory.TokenInfrastructureFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 22/09/2022
 */
@LogAudited
@Named("RestServiceImpl")
public class RestServiceImpl implements IRestService {

    private final TokenInfrastructureFactory serviceFactory;
    @Inject
    public RestServiceImpl(final TokenInfrastructureFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }
    @Inject
    private DateFormatter dateFormatter;

    @Override
    public Response isValidToken(String value) {
        Token token = this.serviceFactory.getServiceFactory().getTokenService().findByValue(value);
        if (token == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("Token not found").build(); // 404 status code
        }
        if (token.getValue().isBlank() || token.getValue().isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Missing token").build(); // 400 status code
        }
        if (token.getExpirationDate().isBefore(LocalDateTime.now())) {
            dateFormatter = new DateFormatter();
            return Response.status(Response.Status.GONE).entity("This token has expired since " + dateFormatter.formatDateToString(token.getExpirationDate())).build(); // 410 status code
        }
        return Response.ok(Response.Status.FOUND).entity(token).build(); // 200 status code
    }

    @Override
    public Response badRequest() {
        String message = "Missing token value";
        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity(message)
                .type(MediaType.TEXT_PLAIN)
                .build();
    }

}

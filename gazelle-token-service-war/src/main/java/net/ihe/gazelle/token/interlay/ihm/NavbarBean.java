
package net.ihe.gazelle.token.interlay.ihm;

import javax.faces.annotation.ManagedProperty;
import javax.inject.Named;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 01/07/2022
 */
@Named
public class NavbarBean {

    @ManagedProperty(value="#{param.pageId}")
    private String pageId;

    private static final String LOGIN_LINK = "/gazelle-token-service/cas/login";

    public String loginLink() {
        return LOGIN_LINK;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String displayPage() {
        if (pageId == null) {
            return "/gazelle-token-service/";
        }
        if (pageId.equals("Tokens")) {
            return "/gazelle-token-service/manager/";
        } else if (pageId.equals("Admin")) {
            return "/gazelle-token-service/manager/admin.xhtml";
        } else if (pageId.equals("Index")) {
            return "/gazelle-token-service/index.xhtml";
        } else {
            return "/gazelle-token-service/index.xhtml";
        }
    }
}
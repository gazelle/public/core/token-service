
package net.ihe.gazelle.token.application.service;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 22/09/2022
 */
public interface IPropertiesService {
    String getVersion();
}

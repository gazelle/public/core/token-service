
package net.ihe.gazelle.token.interlay.service;

import net.ihe.gazelle.token.application.mapper.IPreferenceMapper;
import net.ihe.gazelle.token.application.model.EConstantValue;
import net.ihe.gazelle.token.application.model.PreferenceEntity;
import net.ihe.gazelle.token.application.service.IPreferenceService;
import net.ihe.gazelle.token.domain.entity.Preference;
import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import net.ihe.gazelle.token.interlay.dao.exception.DaoException;
import net.ihe.gazelle.token.interlay.factory.TokenInfrastructureFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 22/09/2022
 */
@LogAudited
@Named("PreferenceServiceImpl")
public class PreferenceServiceImpl implements IPreferenceService {

    private static Logger LOG = LoggerFactory.getLogger(PreferenceServiceImpl.class);
    @Inject
    @Named("PreferenceMapper")
    private IPreferenceMapper preferenceMapper;

    @Named("TokenInfrastructureFactory")
    private final TokenInfrastructureFactory serviceFactory;

    @Inject
    public PreferenceServiceImpl(TokenInfrastructureFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Override
    public Preference getPreference() {
        PreferenceEntity preferenceEntity = this.serviceFactory.getDaoFactory().getPreferenceDao().getPreference(EConstantValue.getPreferenceId());
        return this.preferenceMapper.PreferenceEntityToPreference(preferenceEntity);
    }

    @Override
    public int getTokenDuration() {
        PreferenceEntity preferenceEntity = this.serviceFactory.getDaoFactory().getPreferenceDao().getPreference(EConstantValue.getPreferenceId());
        if (preferenceEntity == null) {
            return 365;
        }
        return preferenceEntity.getTokenDuration();
    }

    @Override
    public void setDuration(int duration) throws DaoException {
        Preference preference = Preference.builder().withId(EConstantValue.getPreferenceId()).withTokenDuration(duration).build();
        PreferenceEntity preferenceEntity = this.serviceFactory.getDaoFactory().getPreferenceDao().getPreference(EConstantValue.getPreferenceId());
        if (preferenceEntity == null) {
            this.serviceFactory.getDaoFactory().getPreferenceDao().save(preferenceMapper.preferenceToPreferenceEntity(preference));
        }
        this.serviceFactory.getDaoFactory().getPreferenceDao().update(preferenceMapper.preferenceToPreferenceEntity(preference));
    }
}

package net.ihe.gazelle.token.domain.entity;

import net.ihe.gazelle.token.domain.entity.Token;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token-service
 * @date 09/06/2022
 */
class TokenTest {

    @Test
    void shouldGetAllTokenInformation() {
        List<String> roles = new ArrayList<>();
        roles.add("role_admin");
        Token token = Token.builder()
                .withValue("test")
                .withUsername("mfortiche")
                .withOrganization("kereval")
                .withCreationDate(LocalDateTime.of(2022, 10,3,8,00))
                .withExpirationDate(LocalDateTime.of(2023, 10,3,8,00))
                .withRoles(roles)
                .build();
        Assertions.assertEquals("Token{value='test', username='mfortiche', organization='kereval', creationDate=2022-10-03T08:00, expirationDate=2023-10-03T08:00, roles=[role_admin]}", token.toString());
    }

    @Test
    void shouldNotBuildWithoutValues() {
        Token token = Token.builder()
                .withValue(null)
                .withUsername(null)
                .withOrganization(null)
                .withCreationDate(null)
                .withExpirationDate(null)
                .withRoles(null)
                .build();
        Assertions.assertNotEquals("vdsfdsfsd", token.getValue());
    }
}

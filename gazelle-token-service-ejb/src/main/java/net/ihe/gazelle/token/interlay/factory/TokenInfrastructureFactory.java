
package net.ihe.gazelle.token.interlay.factory;

import net.ihe.gazelle.token.application.dao.IDaoFactory;
import net.ihe.gazelle.token.application.mapper.IPreferenceMapper;
import net.ihe.gazelle.token.application.mapper.ITokenMapper;
import net.ihe.gazelle.token.application.security.ITokenGenerator;
import net.ihe.gazelle.token.application.service.IServiceFactory;
import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import net.ihe.gazelle.token.interlay.dao.DaoFactoryImpl;
import net.ihe.gazelle.token.interlay.mapper.PreferenceMapperImpl;
import net.ihe.gazelle.token.interlay.mapper.TokenMapperImpl;
import net.ihe.gazelle.token.interlay.security.TokenGeneratorImpl;
import net.ihe.gazelle.token.interlay.service.ServiceFactoryImpl;

import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project GazelleTokenService
 * @date 14/06/2022
 */
@LogAudited
@Named("TokenInfrastructureFactory")
public class TokenInfrastructureFactory {

    @PersistenceContext(unitName = "GTZ_PU")
    private EntityManager entityManager;

    @Produces
    public EntityManager createEntityManager() {
        return entityManager;
    }

    @Produces
    @Named
    public IDaoFactory getDaoFactory() {
        return new DaoFactoryImpl(entityManager);
    }

    @Produces
    @Named
    public IServiceFactory getServiceFactory() {
        return new ServiceFactoryImpl(this);
    }

    @Produces
    @Named
    public ITokenMapper getTokenMapper() {
        return new TokenMapperImpl();
    }

    @Produces
    @Named
    public IPreferenceMapper getPreferenceMapper() {
        return new PreferenceMapperImpl();
    }

    @Produces
    @Named
    public ITokenGenerator getTokenGenerator() {
        return new TokenGeneratorImpl();
    }

}


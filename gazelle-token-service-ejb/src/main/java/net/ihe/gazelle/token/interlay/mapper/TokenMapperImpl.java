
package net.ihe.gazelle.token.interlay.mapper;

import net.ihe.gazelle.token.application.mapper.ITokenMapper;
import net.ihe.gazelle.token.application.model.TokenEntity;
import net.ihe.gazelle.token.domain.entity.Token;

import javax.inject.Named;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project GazelleTokenService
 * @date 14/06/2022
 */
@Named("TokenMapper")
public class TokenMapperImpl implements ITokenMapper {


    @Override
    public Token tokenEntityToToken(TokenEntity tokenEntity) {
        if (tokenEntity != null) {
            return Token.builder()
                    .withValue(tokenEntity.getValue())
                    .withUsername(tokenEntity.getUsername())
                    .withOrganization(tokenEntity.getOrganization())
                    .withCreationDate(tokenEntity.getCreationDate())
                    .withExpirationDate(tokenEntity.getExpirationDate())
                    .withRoles(tokenEntity.getRoles())
                    .build();
        }
        return null;
    }

    @Override
    public TokenEntity tokenToTokenEntity(Token token) {
        return TokenEntity.builder()
                .withValue(token.getValue())
                .withUsername(token.getUsername())
                .withOrganization(token.getOrganization())
                .withCreationDate(token.getCreationDate())
                .withExpirationDate(token.getExpirationDate())
                .withRoles(token.getRoles())
                .build();
    }

    @Override
    public List<Token> tokenEntitiesListToTokenList(List<TokenEntity> tokensEntities) {
        return tokensEntities.stream().map(this::tokenEntityToToken).collect(Collectors.toList());
    }


    @Override
    public List<TokenEntity> tokenListToTokenEntitiesList(List<Token> tokens) {
        return tokens.stream().map(this::tokenToTokenEntity).collect(Collectors.toList());
    }


}

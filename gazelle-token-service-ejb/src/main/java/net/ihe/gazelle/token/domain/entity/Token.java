package net.ihe.gazelle.token.domain.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 12/05/2022
 */
public class Token implements Serializable {

    private final String value;
    private final String username;
    private final String organization;
    private final LocalDateTime creationDate;
    private final LocalDateTime expirationDate;
    private final List<String> roles;

    public Token(TokenBuilder builder) {
        this.value          = builder.value;
        this.username       = builder.username;
        this.organization   = builder.organization;
        this.creationDate   = builder.creationDate;
        this.expirationDate = builder.expirationDate;
        this.roles          = builder.roles;
    }


    public static TokenBuilder builder() {
        return new TokenBuilder();
    }

    public static class TokenBuilder {
        private String value;
        private String username;
        private String organization;
        private LocalDateTime creationDate;
        private LocalDateTime expirationDate;
        private List<String> roles;

        public TokenBuilder withValue(String value) {
            this.value = value;
            return this;
        }

        public TokenBuilder withUsername(String username) {
            this.username = username;
            return this;
        }

        public TokenBuilder withOrganization(String organization) {
            this.organization = organization;
            return this;
        }

        public TokenBuilder withCreationDate(LocalDateTime creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public TokenBuilder withExpirationDate(LocalDateTime expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public TokenBuilder withRoles(List<String> roles) {
            this.roles = roles;
            return this;
        }

        public Token build() {
            return new Token(this);
        }
    }

    public String getValue() {
        return value;
    }

    public String getUsername() {
        return username;
    }

    public String getOrganization() {
        return organization;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public List<String> getRoles() {
        return roles;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Token{");
        sb.append("value='").append(value).append('\'');
        sb.append(", username='").append(username).append('\'');
        sb.append(", organization='").append(organization).append('\'');
        sb.append(", creationDate=").append(creationDate);
        sb.append(", expirationDate=").append(expirationDate);
        sb.append(", roles=").append(roles);
        sb.append('}');
        return sb.toString();
    }
}

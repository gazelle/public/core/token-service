
package net.ihe.gazelle.token.domain.entity;

import java.io.Serializable;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 14/07/2022
 */
public class Preference implements Serializable {

    private String id;
    private int tokenDuration;


    public Preference(PreferenceBuilder builder) {
        this.id                 = builder.id;
        this.tokenDuration      = builder.tokenDuration;
    }

    public Preference() {
    }

    public static PreferenceBuilder builder() {
        return new PreferenceBuilder();
    }

    public static class PreferenceBuilder {

        private String id;
        private int tokenDuration;


        public PreferenceBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public PreferenceBuilder withTokenDuration(int tokenDuration) {
            this.tokenDuration = tokenDuration;
            return this;
        }

        public Preference build() {
            return new Preference(this);
        }
    }

    public String getId() {
        return id;
    }

    public int getTokenDuration() {
        return tokenDuration;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Preference{");
        sb.append("id='").append(id).append('\'');
        sb.append(", tokenDuration=").append(tokenDuration);
        sb.append('}');
        return sb.toString();
    }
}


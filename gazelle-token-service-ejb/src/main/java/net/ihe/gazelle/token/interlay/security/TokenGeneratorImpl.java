/*
 * Gazelle Token Service is an Api for the Gazelle Test Bed
 * Copyright (C) 2006-2022 IHE
 * mailto :claude DOT lusseau AT kereval DOT com
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.token.interlay.security;

import net.ihe.gazelle.token.application.security.ITokenGenerator;
import net.ihe.gazelle.token.interlay.annotation.LogAudited;

import javax.inject.Named;
import java.security.SecureRandom;
import java.util.Base64;

/**
 *
 *
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 12/05/2022
 */
@LogAudited
@Named("TokenGenerationImpl")
public class TokenGeneratorImpl implements ITokenGenerator {
    private final SecureRandom secureRandom = new SecureRandom();
    private final Base64.Encoder base64Encoder = Base64.getUrlEncoder();

    @Override
    public String generateNewToken() {
        byte[] randomBytes = new byte[128];
        secureRandom.nextBytes(randomBytes);
        return base64Encoder.encodeToString(randomBytes);
    }
}

/**
 * Gazelle Token Service is an Api for the Gazelle Test Bed
 * Copyright (C) 2006-2022 IHE
 * mailto :claude DOT lusseau AT kereval DOT com
 * <p>
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.token.interlay.dao;

import net.ihe.gazelle.token.application.model.EConstantValue;
import net.ihe.gazelle.token.application.model.PreferenceEntity;
import org.junit.jupiter.api.*;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 27/09/2022
 */
public class DaoFactoryImplTest {

    private DaoFactoryImpl daoFactory;
    private static final String PERSISTENCE_UNIT_NAME_CONST = "GTZ_TEST";
    private static EntityManagerFactory factory;
    private EntityManager entityManager;
    @BeforeAll
    public static void setUp() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST);
    }

    @AfterAll
    public static void closeAndDropDatabase() {
        // Drop database at sessionFactory closure if hbm2ddl.auto is set to "create-drop"
        factory.close();
    }

    @BeforeEach
    public void initializeEntityManager() {
        entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();
        daoFactory = new DaoFactoryImpl(entityManager);
    }

    @AfterEach
    public void closeEntityManager() {
        entityManager.getTransaction().commit();
        entityManager.close();
    }
    @Test
    void getPreferenceDaoTest() {
        daoFactory = new DaoFactoryImpl(entityManager);
        Assertions.assertNotNull(daoFactory.getPreferenceDao());
    }

    @Test
    void getTokenDaoTest() {
        daoFactory = new DaoFactoryImpl(entityManager);
        Assertions.assertNotNull(daoFactory.getTokenDao());
    }
}


package net.ihe.gazelle.token.application.service;

import net.ihe.gazelle.token.domain.entity.Preference;
import net.ihe.gazelle.token.interlay.dao.exception.DaoException;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 22/09/2022
 */
public interface IPreferenceService {

    int getTokenDuration();
    void setDuration(int duration) throws DaoException;
    Preference getPreference();
}

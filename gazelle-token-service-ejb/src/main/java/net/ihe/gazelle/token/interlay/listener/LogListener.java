
package net.ihe.gazelle.token.interlay.listener;

import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 19/07/2022
 */
@Interceptor
@LogAudited
public class LogListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogListener.class.getName());

    @AroundInvoke
    public Object audit(InvocationContext context) throws Exception {
        LOGGER.info("Calling Method : " + context.getMethod().getName());
        return context.proceed();
    }
}

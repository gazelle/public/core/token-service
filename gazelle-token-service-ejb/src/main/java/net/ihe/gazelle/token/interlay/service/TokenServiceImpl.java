
package net.ihe.gazelle.token.interlay.service;

import net.ihe.gazelle.token.application.exception.UserNotFoundException;
import net.ihe.gazelle.token.application.model.TokenEntity;
import net.ihe.gazelle.token.application.model.TokenValidator;
import net.ihe.gazelle.token.application.service.ITokenService;
import net.ihe.gazelle.token.domain.entity.Preference;
import net.ihe.gazelle.token.domain.entity.Token;
import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import net.ihe.gazelle.token.interlay.factory.TokenInfrastructureFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 22/09/2022
 */
@LogAudited
@Named("TokenServiceImpl")
public class TokenServiceImpl implements ITokenService {

    private final TokenInfrastructureFactory serviceFactory;

    @Inject
    public TokenServiceImpl(TokenInfrastructureFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Override
    public void saveToken(String username, String organization, List<String> roles) throws UserNotFoundException {
        int preferenceDuration = this.serviceFactory.getServiceFactory().getPreferenceService().getTokenDuration();
        int durationValue = 365;
        if (preferenceDuration != 0) {
            durationValue = preferenceDuration;
        }
        List<TokenEntity> tokens = this.serviceFactory.getDaoFactory().getTokenDao().getTokensPerOwner(username);
        if (tokens.stream().filter(t -> t.getExpirationDate().isAfter(LocalDateTime.now(ZoneId.systemDefault()))).findFirst().isEmpty()) {
            var tokenToSave = Token.builder()
                    .withValue(serviceFactory.getTokenGenerator().generateNewToken())
                    .withUsername(username)
                    .withOrganization(organization)
                    .withCreationDate(LocalDateTime.now(ZoneId.systemDefault()))
                    .withExpirationDate(LocalDateTime.now().plusDays(durationValue))
                    .withRoles(roles)
                    .build();
            TokenValidator.validateCreateToken(tokenToSave);
            this.serviceFactory.getDaoFactory().getTokenDao().create(serviceFactory.getTokenMapper().tokenToTokenEntity(tokenToSave));
        }
    }

    @Override
    public Token findByValue(String value) {
        TokenEntity tokenEntity = this.serviceFactory.getDaoFactory().getTokenDao().getTokenByValue(value);
        return serviceFactory.getTokenMapper().tokenEntityToToken(tokenEntity);
    }

    @Override
    public List<Token> getUserTokens(String username) {
        List<TokenEntity> tokensEntityList = this.serviceFactory.getDaoFactory().getTokenDao().getTokensPerOwner(username);
        return serviceFactory.getTokenMapper().tokenEntitiesListToTokenList(tokensEntityList);
    }

    @Override
    public List<Token> getAllTokenPerUser(String username) {
        List<TokenEntity> tokensEntityList = this.serviceFactory.getDaoFactory().getTokenDao().getTokensPerOwner(username);
        return serviceFactory.getTokenMapper().tokenEntitiesListToTokenList(tokensEntityList);
    }

    @Override
    public Optional<Token> getUniqueActiveToken(String username) {
        List<Token> tokens = this.getUserTokens(username);
        return tokens
                .stream()
                .filter(token -> token.getExpirationDate().isAfter(LocalDateTime.now()))
                .findFirst();
    }

    @Override
    public void deactivateValidToken(String value) {
        LocalDateTime currentTime = LocalDateTime.now(ZoneId.systemDefault());
        LocalDateTime expirationDate = currentTime.minusMinutes(2);

        this.serviceFactory.getDaoFactory().getTokenDao().deactivateValidToken(value, expirationDate);
    }

    @Override
    public void deleteOldTokensByUser(String username) {
        this.serviceFactory.getDaoFactory().getTokenDao().deleteOldTokens(username);
    }
}
